﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public void goToPlayScreen() {
		string sceneName = getSceneNameForPlayScreen();
		SceneManager.LoadScene(sceneName);
	}

	private string getSceneNameForPlayScreen() {
		return "Play Screen";
	}
	
	public void quitTheGame() {
		Application.Quit();
	}
}
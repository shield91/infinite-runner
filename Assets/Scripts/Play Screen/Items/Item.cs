﻿using UnityEngine;

public abstract class Item : MonoBehaviour {
	private Collider2D initiator;
	
	private void Start() {
		resetInitiator();
		resetItemName();
	}

	private void resetInitiator() {
		Collider2D VALUE = null;
		setInitiator(VALUE);
	}

	private void setInitiator(Collider2D value) {
		initiator = value;
	}

	private void resetItemName() {
		name = getItemName();
	}

	protected abstract string getItemName();
	
	private void OnTriggerEnter2D(Collider2D c) {
		retrieveInitiatorFrom(c);
		processInteraction();
	}

	private void retrieveInitiatorFrom(Collider2D c) {
		var value = c;
		setInitiator(value);
	}

	private void processInteraction() {
		if (itemIsInteractingWithPlayer()) {
			processInteractionBetweenItemAndPlayer();
		}
	}

	private bool itemIsInteractingWithPlayer() {
		const string VALUE = "Body";
		var initiatorName = getInitiatorName();
		return initiatorName.Equals(VALUE);
	}

	private string getInitiatorName() {
		var originator = getInitiator();
		return originator.name;
	}

	private Collider2D getInitiator() {
		return initiator;
	}

	protected abstract void processInteractionBetweenItemAndPlayer();

	private void OnCollisionEnter2D(Collision2D c) {
		retrieveInitiatorFrom(c);
		processInteraction();
	}

//	private void LateUpdate() {
//		if (GetComponent<Obstacle> () != null) {
//			if (Physics2D.IsTouchingLayers(GetComponent<Collider2D>(), 
//				LayerMask.GetMask("Player"))) {
//
//				Debug.Log (Time.deltaTime);
//				setInitiator(GetComponentInParent<PlayScreen>().
//					transform.Find("Player/Body").GetComponent<Collider2D>());
//				processInteraction();
//			}
//		}
//	}

	private void retrieveInitiatorFrom(Collision2D c) {
		var value = c.collider;
		setInitiator(value);
	}
}
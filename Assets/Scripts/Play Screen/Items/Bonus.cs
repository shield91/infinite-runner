public class Bonus : Item {
	protected override string getItemName() {
		return "Bonus";
	}
	
	protected override void processInteractionBetweenItemAndPlayer() {
		updateScore();
		destroyThisItem();
	}

	private void updateScore() {
		var ps = GetComponentInParent<PlayScreen>();
		ps.updateScore();
	}

	private void destroyThisItem() {
		var obj = gameObject;
		Destroy(obj);
	}
}
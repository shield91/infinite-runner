public class Obstacle : Item {
	protected override string getItemName() {
		return "Obstacle";
	}

	protected override void processInteractionBetweenItemAndPlayer() {
		endGame();
		deleteScriptFromPlayer();
		deleteThisScript();
	}

	private void endGame() {
		var ps = GetComponentInParent<PlayScreen>();
		ps.endGame();
	}

	private void deleteScriptFromPlayer() {
		const string NAME = "Player";
		var ps = GetComponentInParent<PlayScreen>();
		var gameWindow = ps.transform;
		var player = gameWindow.Find(NAME);
		var obj = player.GetComponent<Player>();
		Destroy(obj);
	}

	private void deleteThisScript() {
		var obj = this;
		Destroy(obj);
	}
}
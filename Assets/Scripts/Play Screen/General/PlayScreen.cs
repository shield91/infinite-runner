﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayScreen : MonoBehaviour {
	private void Start() {
		putGameOnPause();
		hidePauseWindow();
		scaleThisScreen();

		if (getScaleMultiplier () < 1f) {
			transform.Find ("Player").GetComponent<CapsuleCollider2D> ()
				.direction = CapsuleDirection2D.Vertical;
			transform.Find ("Player/Body").GetComponent<CapsuleCollider2D> ()
				.direction = CapsuleDirection2D.Vertical;
		}
	}

	private void putGameOnPause() {
		const float VALUE = 0f;
		Time.timeScale = VALUE;
	}

	private void hidePauseWindow() {
		var element = getPauseWindow();
		hide(element);
	}

	private GameObject getPauseWindow() {
		const string NAME = "Pop-Up Window";
		return get(NAME);
	}

	private GameObject get(string name) {
		var t = getTransformFor(name);
		return t.gameObject;
	}

	private Transform getTransformFor(string name) {
		var go = getCanvas();
		var t = go.transform;
		return t.Find(name);
	}

	private GameObject getCanvas() {
		const string TAG = "Canvas";
		return GameObject.FindWithTag(TAG);
	}

	private void hide(GameObject element) {
		const bool VALUE = false;
		element.SetActive(VALUE);
	}

	private void scaleThisScreen() {
		var oldScale = transform.localScale;
		var x = getScaleMultiplier();
		var y = oldScale.y;
		var z = oldScale.z;
		var newScale = new Vector3(x, y, z);
		transform.localScale = newScale;
	}

	private float getScaleMultiplier() {
		const float TARGET_WIDTH = 1080f;
		const float TARGET_HEIGHT = 1920f;
		const float TARGET_RATIO = TARGET_WIDTH / TARGET_HEIGHT;
		float actualRatio = getActualRatio();
		return actualRatio / TARGET_RATIO;
	}

	private float getActualRatio() {
		float actualWidth = Screen.width;
		float actualHeight = Screen.height;
		return actualWidth / actualHeight;
	}

	public void goToMainMenu() {
		makePreparationsForGoingToMainMenu();
		loadMainMenu();
	}

	private void makePreparationsForGoingToMainMenu() {
		putGameOutOfPause();
	}

	public void putGameOutOfPause() {
		const float VALUE = 1f;
		Time.timeScale = VALUE;
	}

	private void loadMainMenu() {
		string sceneName = getMainMenuSceneName();
		SceneManager.LoadScene(sceneName);
	}

	private string getMainMenuSceneName() {
		return "Main Menu";
	}

	public void resumeGame() {
		hidePauseWindow();
		showPauseButton();
		putGameOutOfPause();
		enablePlayerControl();
	}

	public void showPauseButton() {
		var element = getPauseButton();
		show(element);
	}

	private GameObject getPauseButton() {
		const string NAME = "Pause";
		return get(NAME);
	}

	private void show(GameObject element) {
		const bool VALUE = true;
		element.SetActive(VALUE);
	}

	private void enablePlayerControl() {
		const bool VALUE = true;
		setPlayerControlToBeEnabled(VALUE);
	}

	private void setPlayerControlToBeEnabled(bool value) {
		var collider = getPlayerCollider();
		collider.enabled = value;
	}

	private Collider2D getPlayerCollider() {
		var player = getPlayer();
		return player.GetComponent<Collider2D>();
	}

	private Transform getPlayer() {
		const string NAME = "Player";
		return transform.Find(NAME);
	}

	public void pauseGame() {
		putGameOnPause();
		hidePauseButton();
		showPauseWindow();
		disablePlayerControl();
	}

	private void hidePauseButton() {
		var element = getPauseButton();
		hide(element);
	}

	private void showPauseWindow() {
		var element = getPauseWindow();
		show(element);
	}

	private void disablePlayerControl() {
		const bool VALUE = false;
		setPlayerControlToBeEnabled(VALUE);
	}

	public void updateScore() {
		var tm = getTextMeshForScore();
		var newScore = getNewScore();
		tm.text = newScore.ToString();
	}

	private TextMesh getTextMeshForScore() {
		var t = getTransformForScoreValue();
		return t.GetComponent<TextMesh>();
	}

	private Transform getTransformForScoreValue() {
		const string NAME = "Score/Value";
		return transform.Find(NAME);
	}

	private int getNewScore() {
		int oldScore = getOldScore();
		int increment = getScoreIncrement();
		return oldScore + increment;
	}

	private int getOldScore() {
		var tm = getTextMeshForScore();
		var s = tm.text;
		return int.Parse(s);
	}

	private int getScoreIncrement() {
		return 1;
	}

	public void endGame() {
		putGameOnPause();
		hidePauseButton();
		showGameCompleteWindow();
	}

	private void showGameCompleteWindow() {
		showPauseWindow();
		turnPauseWindowIntoGameCompleteOne();
	}

	private void turnPauseWindowIntoGameCompleteOne() {
		turnPauseWindowHeaderIntoGameCompleteOne();
		showTryAgainButton();
	}

	private void turnPauseWindowHeaderIntoGameCompleteOne() {
		var t = getTextComponentForPopUpWindowHeader();
		var title = getTitleForGameCompleteWindow();
		t.text = title;
	}

	private Text getTextComponentForPopUpWindowHeader() {
		var header = getPopUpWindowHeader();
		return header.GetComponent<Text>();
	}

	private GameObject getPopUpWindowHeader() {
		const string NAME = "Pop-Up Window/Header";
		return get(NAME);
	}

	private string getTitleForGameCompleteWindow() {
		return "Game Complete";
	}

	private void showTryAgainButton() {
		var element = getTryAgainButton();
		show(element);
	}

	private GameObject getTryAgainButton() {
		const string NAME = "Pop-Up Window/Try Again";
		return get(NAME);
	}

	public void reloadGame() {
		var sceneName = getLoadedSceneName();
		SceneManager.LoadScene(sceneName);
	}

	private string getLoadedSceneName() {
		var loadedScene = SceneManager.GetActiveScene();
		return loadedScene.name;
	}

	public bool gameIsPaused() {
		int timeScale = getRoundedTimeScale();
		int value = getValueForPausedGame();
		return timeScale == value;
	}

	private int getRoundedTimeScale() {
		var f = Time.timeScale;
		return Mathf.RoundToInt(f);
	}

	private int getValueForPausedGame() {
		return 0;
	}
}
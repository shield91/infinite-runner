﻿using UnityEngine;

public class TrackManager : MonoBehaviour {
	private void Start() {
		while(trackIsBeingConstructed()) {
			addNewTileToTrack();
		}
	}

	private bool trackIsBeingConstructed() {
		int number = getTilesNumber();
		int maximum = getTilesNumberMaximum();
		return number < maximum;
	}

	private int getTilesNumber() {
		return transform.childCount;
	}

	private int getTilesNumberMaximum() {
		return 3;
	}

	private void addNewTileToTrack() {
		spawnNewTile();
	}
	
	public void spawnNewTile() {
		var parent = transform;
		var t = getTransformForInstantiatedTile();
		var position = getPositionForNewTile();
		t.position = parent.TransformPoint(position);
		t.SetParent(parent);


		t.localScale = new Vector3(5.8f,t.localScale.y,t.localScale.z);
	}

	private Transform getTransformForInstantiatedTile() {
		var original = getOriginalForNewTile();
		var o = Instantiate(original);
		var go = (GameObject) o;
		return go.transform;
	}

	private Object getOriginalForNewTile() {
		const string PATH = "Tile";
		return Resources.Load(PATH);
	}

	private Vector3 getPositionForNewTile() {
		if (tileHasItsForerunner()) {
			return getPositionForThisTile();
		} else {
			return getPositionForTheFirstTile();
		}
	}

	private bool tileHasItsForerunner() {
		int value = getValueForTileHavingItsForerunner();
		int index = getLastTileIndex();
		return index >= value;
	}

	private int getValueForTileHavingItsForerunner() {
		return 0;
	}

	private int getLastTileIndex() {
		int tilesQuantity = getTilesNumber();
		int difference = getDifferenceBetweenTilesQuantityAndLastTileIndex();
		return tilesQuantity - difference;
	}

	private int getDifferenceBetweenTilesQuantityAndLastTileIndex() {
		return 1;
	}

	private Vector3 getPositionForThisTile() {
		var lastTilePosition = getLastTilePosition();
		var movement = getMovementForThisTile();
		return lastTilePosition + movement;
	}

	private Vector3 getLastTilePosition() {
		var lastTile = getLastTile();
		return lastTile.localPosition;
	}

	private Transform getLastTile() {
		var index = getLastTileIndex();
		return transform.GetChild(index);
	}

	private Vector3 getMovementForThisTile() {
		var direction = getDirectionForThisTile();
		return getMovementFor(direction);
	}

	private Vector3 getDirectionForThisTile() {
		return Vector3.up;
	}

	private Vector3 getMovementFor(Vector3 direction) {
		var multiplier = getMultiplierForTileMovement();
		return direction * multiplier;
	}

	private float getMultiplierForTileMovement() {
		return 11f;
	}

	private Vector3 getPositionForTheFirstTile() {
		var direction = getDirectionForTheFirstTile();
		return getMovementFor(direction);
	}

	private Vector3 getDirectionForTheFirstTile() {
		return Vector3.down;
	}
}
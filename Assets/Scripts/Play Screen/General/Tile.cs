using UnityEngine;

public class Tile : MonoBehaviour {
	private void Update() {
		if (tileIsOutOfPlayerSight()) {
			updateTrack();
		} else {
			continueMotion();
		}
	}

	private bool tileIsOutOfPlayerSight() {
		float y = getTilePositionOnY();
		float value = getValueForTileBeingOutOfPlayerSight();
		return y < value;
	}

	private float getTilePositionOnY() {
		var position = transform.localPosition;
		return position.y;
	}

	private float getValueForTileBeingOutOfPlayerSight() {
		return -12f;
	}

	private void updateTrack() {
		addNewTileToTrack();
		deleteThisTile();
	}

	private void addNewTileToTrack() {
		var tm = GetComponentInParent<TrackManager>();
		tm.spawnNewTile();
	}

	private void deleteThisTile() {
		var obj = gameObject;
		Destroy(obj);
	}

	private void continueMotion() {
		var movement = getMovement();
		transform.localPosition += movement;
	}

	private Vector3 getMovement() {
		var direction = getDirection();
		var multiplier = getMultiplierForTileDirection();
		return direction * multiplier;
	}

	private Vector3 getDirection() {
		return Vector3.down;
	}

	private float getMultiplierForTileDirection() {
		float speed = getSpeedForTile();
		float deltaTime = getDeltaTime();
		return deltaTime * speed;
	}

	private float getSpeedForTile() {
		return 3f;
	}

	private float getDeltaTime() {
		return Time.deltaTime;
	}
}
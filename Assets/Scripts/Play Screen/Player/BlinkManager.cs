using UnityEngine;

namespace AssemblyCSharp
{
	public class BlinkManager {
		private Transform requester;
		private int currentAnimatorIndex;
		
		public BlinkManager(Transform suppliant) {
			setRequester(suppliant);
			resetCurrentAnimatorIndex();
		}

		private void setRequester(Transform value) {
			requester = value;
		}
		
		private void resetCurrentAnimatorIndex() {
			const int VALUE = 0;
			setCurrentAnimatorIndex(VALUE);
		}
		
		private void setCurrentAnimatorIndex(int value) {
			currentAnimatorIndex = value;
		}
		
		public void disableBlink() {
			resetCurrentAnimatorIndex();
			disableAnimators();
		}
		
		private void disableAnimators() {
			while (thereAreAnimatorsNeedToBeProcessed()) {
				disableCurrentAnimator();
			}
		}
		
		private bool thereAreAnimatorsNeedToBeProcessed() {
			int i = getCurrentAnimatorIndex();
			int n = getAnimatorsQuantity();
			return i < n;
		}
		
		private int getCurrentAnimatorIndex() {
			return currentAnimatorIndex;
		}
		
		private int getAnimatorsQuantity() {
			var suppliant = getRequester();
			return suppliant.childCount;
		}

		private Transform getRequester() {
			return requester;
		}
		
		private void disableCurrentAnimator() {
			disableAnimator();
			returnAnimatorOwnerItsUsualColor();
			returnAnimatorToItsInitialState();
			moveOnToTheNextAnimator();
		}

		private void disableAnimator() {
			const bool VALUE = false;
			var animator = getCurrentAnimator();
			animator.enabled = VALUE;
		}

		private Animator getCurrentAnimator() {
			var suppliant = getRequester();
			var animators = suppliant.GetComponentsInChildren<Animator>();
			var index = getCurrentAnimatorIndex();
			return animators[index];
		}
		
		private void returnAnimatorOwnerItsUsualColor() {
			var owner = getCurrentAnimatorOwner();
			var renderer = owner.GetComponent<SpriteRenderer>();
			var rendererColor = renderer.color;
			var r = rendererColor.r;
			var g = rendererColor.g;
			var b = rendererColor.b;
			var color = new Color(r, g, b);
			renderer.color = color;
		}
		
		private Transform getCurrentAnimatorOwner() {
			var animator = getCurrentAnimator();
			return animator.transform;
		}
		
		private void returnAnimatorToItsInitialState() {
			const string STATE_NAME = "";
			const int LAYER = 0;
			const float NORMALIZED_TIME = 0f;
			var animator = getCurrentAnimator();
			animator.Play(STATE_NAME, LAYER, NORMALIZED_TIME);
		}
		
		private void moveOnToTheNextAnimator() {
			const int INCREMENT = 1;
			int index = getCurrentAnimatorIndex();
			int value = index + INCREMENT;
			setCurrentAnimatorIndex(value);
		}
	}
}
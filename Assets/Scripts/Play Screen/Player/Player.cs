﻿using UnityEngine;
using AssemblyCSharp;

public class Player : MonoBehaviour {
	private void OnMouseDown() {
		if (playerIsGoingToMove()) {
			startPlayerMotion();
		}
	}

	private bool playerIsGoingToMove() {
		var ps = GetComponentInParent<PlayScreen>();
		return ps.gameIsPaused();
	}

	private void startPlayerMotion() {
		putGameOutOfPause();
		disablePlayerBlink();
		showPauseButton();
	}

	private void putGameOutOfPause() {
		var ps = GetComponentInParent<PlayScreen>();
		ps.putGameOutOfPause();
	}

	private void disablePlayerBlink() {
		var suppliant = transform;
		var manager = new BlinkManager(suppliant);
		manager.disableBlink();
	}

	private void showPauseButton() {
		var ps = GetComponentInParent<PlayScreen>();
		ps.showPauseButton();
	}		

	private void OnMouseDrag() {
		var x = getPositionForPlayerOnX();
		var y = getPositionForPlayerOnY();
		var z = getPositionForPlayerOnZ();



//		transform.position = new Vector3(x, y, z);

		var newPosition = new Vector3(x, y, z);
		var distance = Vector3.Distance (transform.position, newPosition);
		var body = transform.Find ("Body");
		//var distance = Vector3.Distance (body.position, newPosition);
		var capsuleCollider = body.GetComponent<CapsuleCollider2D> ();
//		var distance = Vector3.Distance (body.position, newPosition+body.localPosition 
//			+ (Vector3) capsuleCollider.offset);
//		var distance = Vector3.Distance (body.position + (Vector3) capsuleCollider.offset, 
//			newPosition + body.localPosition + (Vector3) capsuleCollider.offset);
		

//		var DISTANCE = Mathf.Max (distance -
//			Vector3.Distance (transform.position, body.position + (Vector3)capsuleCollider.offset)
//			, 0.001f);
//		var DISTANCE = distance - Vector3.Distance (transform.position, 
//			body.position + (Vector3)capsuleCollider.offset);

		//try this (можна й знизу вгору швидко (а не тільки зверху вниз))

		var DISTANCE = distance;
		//https://docs.unity3d.com/ScriptReference/Bounds.ClosestPoint.html
		//https://docs.unity3d.com/ScriptReference/CapsuleCollider2D.html
		//https://docs.unity3d.com/ScriptReference/Physics2D.OverlapCapsule.html
		//https://docs.unity3d.com/ScriptReference/Physics2D.CapsuleCast.html
		//https://docs.unity3d.com/ScriptReference/RaycastHit2D-normal.html
		//https://docs.unity3d.com/ScriptReference/RaycastHit2D-centroid.html
		//https://docs.unity3d.com/ScriptReference/RaycastHit2D.html


		Debug.Log (newPosition);


//		if (distance > 3.5f) {

		if (DISTANCE > 1f) {
			bool collision = false;

			var origin = body.position + (Vector3) capsuleCollider.offset;
//			var origin = body.position + (Vector3)capsuleCollider.offset
//			             + Vector3.down * 3f * Time.deltaTime;
//			var origin = transform.position;
//			var size = capsuleCollider.size;
			var sizeMultiplier = 0.5f;
			var size = capsuleCollider.size * sizeMultiplier;
			var capsuleDirection = capsuleCollider.direction;
			var angle = 0f;
			//var direction = newPosition - transform.position + Vector3.one* 0.1f;
//			var direction = newPosition - body.position;
//			var direction = newPosition + body.localPosition + (Vector3)capsuleCollider.offset
//				- body.position;
//			var direction = newPosition + body.localPosition + (Vector3)capsuleCollider.offset
//				- origin;
//			var direction = newPosition + body.localPosition + (Vector3) capsuleCollider.offset
//				- origin;
			var direction = newPosition - transform.position;

//			var direction = newPosition + Vector3.down*3f*Time.deltaTime - transform.position;

			// точка зачіпається за перешкоду, навіть якщо між перешкодою та точкою є відстань
			// 
			//one hit
//			var hit = Physics2D.CapsuleCast(origin, size, capsuleDirection,	angle,
//						direction, DISTANCE,
//							LayerMask.GetMask("Player"));
//
//			collision = hit.collider != null;
//
//			if (collision) {
//				Debug.Log ("Origin: " + origin + " Size: " + size + 
//					    	" Direction: " + direction + " Distance: " + DISTANCE + 
//				" Hit point: " + hit.point);
//				transform.position = hit.point;
//			} else {
//				transform.position = newPosition;		
//			}

			//many hits
			var hits = Physics2D.CapsuleCastAll (origin, size, capsuleDirection, angle, direction,
				DISTANCE);

			var obstacleIndex = 0;
			for (int i = 0; i < hits.Length && !collision; ++i) {
				var colliderName = hits [i].collider.gameObject.name;
				collision = !colliderName.Equals ("Player") && !colliderName.Equals ("Body");
				obstacleIndex = i;
			}				

			if (collision) {
				Debug.Log ("New Position: " + newPosition +" Origin: " + origin + " Size: " + size + 
					" Direction: " + direction + " Distance: " + DISTANCE + 
					" Hit point: " + hits [obstacleIndex].point +
					" Transform: " + transform.position +
					" Body: " + body.position);

				var hitsCentroid = hits [obstacleIndex].centroid;
				var hitsPoint = hits[obstacleIndex].point;

				var downMultiplier = 0.27f;
				var upMultiplier = 0.26f;

				transform.position = 
				//hits [obstacleIndex].point + new Vector2 (-0.003f, 0.242f);
					//hits [obstacleIndex].point;
					//hits [obstacleIndex].point - (Vector2)transform.position.normalized/2f;
					//hits [obstacleIndex].point - (Vector2)origin.normalized*0.000002f;

					//врізається майже досконало знизу, але неможливо врізатись зверху
//					hits [obstacleIndex].point - (hits [obstacleIndex].point-
//						(Vector2)body.position).normalized/2f;

//					hits [obstacleIndex].point + (hits [obstacleIndex].point-
//						(Vector2)body.position).normalized/2f;

//					hits[obstacleIndex].centroid + 
//					Vector2.Min((hits[obstacleIndex].point - hits[obstacleIndex].centroid).normalized*0.25f
//						,/*Vector2.zero*/Vector2.down*0.25f);



					hitsCentroid + 
					Vector2.Min((hitsPoint - hitsCentroid).normalized*downMultiplier
						,/*Vector2.zero Vector2.down*/
						(hitsCentroid - hitsPoint).normalized*upMultiplier);


			} else {
				transform.position = newPosition;
			}				

			//при дуже різких рухах точка починає рухатись ривками й не встигає пройти крізь паркан
			//за цей час паркан встигає зникнути за межами екрану
//			var futurePosition = Vector3.Lerp (transform.position, newPosition, 0f);
//			for (float t = 0f; t < 1f && !collision; t += 0.1f) {
//				futurePosition = Vector3.Lerp (transform.position, newPosition, t);
//
//				//replace transform.position with futurePosition
//				var colliders = Physics2D.OverlapCapsuleAll (
//					(Vector2)futurePosition + (Vector2)body.localPosition+capsuleCollider.offset,
//							size,
//							capsuleDirection,
//							angle);
//				for (int i = 0; i < colliders.Length && !collision; ++i) {
//					var colliderName = colliders [i].gameObject.name;
//					collision = !colliderName.Equals("Body") 
//						&& !colliderName.Equals("Player");
//				}
//				//collision = colliders.Length > 2;
//
//				Debug.Log ("Future position: " + futurePosition);
////				Debug.Log (futurePosition + " " + " Length: " + colliders.Length);
////				for (int i = 0; i < colliders.Length; ++i) {
////					Debug.Log (colliders [i].gameObject.name);
////				}
//			}
//			transform.position = futurePosition;
//
		} else {
			transform.position = newPosition;
		}

		// підхід Васі
//		//float speed = 50f / 2f ;
//		float speed = 50f;
//		transform.position = Vector3.MoveTowards (
//			transform.position,
//			new Vector3 (x, y, z), Time.deltaTime * speed);



	}

	private float getPositionForPlayerOnX() {
		const float OFFSET = 0.7f;
		var bounds = getTrackBounds();
		var inputPosition = getInputPositionInWorldSpace();
		var inputPositionX = inputPosition.x;
		var leftBound = bounds.min;
		var rightBound = bounds.max;
		var leftBoundX = leftBound.x;
		var rightBoundX = rightBound.x;
		var fa = leftBoundX + OFFSET;
		var fb = rightBoundX - OFFSET;
		var fc = Mathf.Max(fa, inputPositionX);
		return Mathf.Min(fc, fb);
	}

	private Bounds getTrackBounds() {
		var ps = GetComponentInParent<PlayScreen>();
		var go = ps.gameObject;
		var t = go.GetComponentInChildren<Tile>();
		var tile = t.transform;
		var bc = tile.GetComponent<BoxCollider>();
		return bc.bounds;
	}

	private Vector3 getInputPositionInWorldSpace() {
		var mainCamera = Camera.main;
		var position = Input.mousePosition;
		return mainCamera.ScreenToWorldPoint(position);
	}

	private float getPositionForPlayerOnY() {
		const float OFFSET = 0.7f;
		const float LOWER_BOUND_Y = -5f;
		const float UPPER_BOUND_Y = 3.4f;
		var inputPosition = getInputPositionInWorldSpace();

		//var inputPositionY = inputPosition.y;
		var inputPositionY = inputPosition.y +0.412f;


		var fa = LOWER_BOUND_Y + OFFSET;
		var fb = UPPER_BOUND_Y - OFFSET;
		var fc = Mathf.Max(fa, inputPositionY);
		return Mathf.Min(fc, fb);
	}

	private float getPositionForPlayerOnZ() {
		var position = transform.position;
		return position.z;
	}
}